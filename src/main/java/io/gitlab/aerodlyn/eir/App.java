package io.gitlab.aerodlyn.eir;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import lombok.extern.flogger.Flogger;

@Flogger
public class App 
{
    public static void main (final String [] args)
    {
        EirParser parser = new EirParser (null);
        Visitor visitor = new Visitor ();
        
        try
        {
            BufferedReader br = new BufferedReader (new InputStreamReader (System.in));
            
            String line = null;
            while ((line = br.readLine ()) != null)
            {
                parser.setTokenStream (new CommonTokenStream (new EirLexer (new ANTLRInputStream (line))));
                log.atInfo ().log (String.valueOf (visitor.visit (parser.program ())));
            }
        }
        
        catch (final IOException ex)
            { ex.printStackTrace (); }
    }
}
