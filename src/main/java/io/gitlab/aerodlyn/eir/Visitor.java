package io.gitlab.aerodlyn.eir;

import io.gitlab.aerodlyn.eir.EirParser.AddExpressionContext;
import io.gitlab.aerodlyn.eir.EirParser.BoolExpressionContext;
import io.gitlab.aerodlyn.eir.EirParser.ConstStatementContext;
import io.gitlab.aerodlyn.eir.EirParser.GroupedExpressionContext;
import io.gitlab.aerodlyn.eir.EirParser.IdentifierExpressionContext;
import io.gitlab.aerodlyn.eir.EirParser.LetStatementContext;
import io.gitlab.aerodlyn.eir.EirParser.MultExpressionContext;
import io.gitlab.aerodlyn.eir.EirParser.NumberExpressionContext;
import io.gitlab.aerodlyn.eir.EirParser.PowExpressionContext;
import io.gitlab.aerodlyn.eir.EirParser.ProgramContext;
import io.gitlab.aerodlyn.eir.EirParser.ReassignmentStatementContext;
import io.gitlab.aerodlyn.eir.EirParser.TypedStatementContext;
import io.gitlab.aerodlyn.eir.EirParser.UnaryExpressionContext;
import io.gitlab.aerodlyn.eir.Parsers.ExpressionParser;
import io.gitlab.aerodlyn.eir.Parsers.StatementParser;
import io.gitlab.aerodlyn.eir.Types.Boolean;
import io.gitlab.aerodlyn.eir.Types.Number;
import io.gitlab.aerodlyn.eir.Types.Value;
import io.gitlab.aerodlyn.eir.Utils.Scope;

import lombok.Getter;

/**
 * Handles evaluating each line of Eir code sequentially.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.10.01
 */
public final class Visitor extends EirBaseVisitor <Value <?>>
{
    @Getter
    private Scope               scope;  
    
    private ExpressionParser    expression;
    private StatementParser     statement;
    
    public Visitor ()
    { 
        scope = new Scope ();
        
        expression = new ExpressionParser (this); 
        statement = new StatementParser (this);
    }
    
    @Override
    public Boolean visitBoolExpression (final BoolExpressionContext ctx)
        { return expression.evaluateBoolExpression (ctx); }
        
    @Override
    public Number visitNumberExpression (final NumberExpressionContext ctx)
        { return expression.evaluateNumberExpression (ctx); }
    
    @Override
    public Value <?> visitAddExpression (final AddExpressionContext ctx)
        { return expression.evaluateAddExpression (ctx); }

    @Override
    public Value <?> visitConstStatement (final ConstStatementContext ctx)
        { return statement.evaluateConstStatement (ctx); }
    
    @Override
    public Value <?> visitGroupedExpression (final GroupedExpressionContext ctx)
        { return expression.evaluateGroupedExpression (ctx); }
        
    @Override
    public Value <?> visitIdentifierExpression (final IdentifierExpressionContext ctx)
        { return expression.evaluateIdentifierExpression (ctx); }
        
    @Override
    public Value <?> visitLetStatement (final LetStatementContext ctx)
        { return statement.evaluateLetStatement (ctx); }
    
    @Override
    public Value <?> visitMultExpression (final MultExpressionContext ctx)
        { return expression.evaluateMultExpression (ctx); }
    
    @Override
    public Value <?> visitPowExpression (final PowExpressionContext ctx)
        { return expression.evaluatePowExpression (ctx); }
    
    @Override 
    public Value <?> visitProgram (final ProgramContext ctx) 
        { return visitChildren (ctx); }
        
    @Override
    public Value <?> visitReassignmentStatement (final ReassignmentStatementContext ctx)
        { return statement.evaluateReassignmentStatement (ctx); }
        
    @Override
    public Value <?> visitTypedStatement (final TypedStatementContext ctx)
        { return statement.evaluateTypedStatement (ctx); }
        
    @Override
    public Value <?> visitUnaryExpression (final UnaryExpressionContext ctx)
        { return expression.evaluateUnaryExpression (ctx); }
}
