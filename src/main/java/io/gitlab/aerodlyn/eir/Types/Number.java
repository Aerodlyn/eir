package io.gitlab.aerodlyn.eir.Types;

import io.gitlab.aerodlyn.eir.Exceptions.OperationNotSupportedException;
import io.gitlab.aerodlyn.eir.Utils.Operator;

/**
 * Represents the basic {@link Number} type of Eir, which is not directly accessible but contains
 *  definitions for operations.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.25
 */
public abstract class Number extends Value <java.lang.Number>
{
    /**
     * Constructs a new {@link Number} object with the given initial value.
     * 
     * @param initial - The initial value to set this instance's internal value
     */
    protected Number (final java.lang.Number initial)
        { internal = initial; }
    
    @Override
    public final Boolean logicalNot ()
        { return new Boolean (java.lang.Double.compare (internal.doubleValue (), 0d) == 0); }
    
    @Override
    public final Integer bitwiseNot ()
        { return new Integer (~internal.intValue ()); }

    @Override
    public final Number absolute ()
    { 
        return this instanceof Float 
            ? new Float (Math.abs (internal.doubleValue ())) 
            : new Integer (Math.abs (internal.intValue ()));
    }
        
    @Override
    public final Number add (final Value <?> value) 
    {
        if (!(value instanceof Number))
            throw new OperationNotSupportedException (Operator.ADDITION, value.getClass ());
            
        java.lang.Number lNumber = this.internal,
                         rNumber = ((Number) value).internal;
               
        if (this instanceof Float)
        {
            if (value instanceof Float)
                return new Float (lNumber.doubleValue () + rNumber.doubleValue ());
                
            else
                return new Float (lNumber.doubleValue () + rNumber.intValue ());
        }
        
        else if (value instanceof Float)
            return new Float (lNumber.intValue () + rNumber.doubleValue ());
            
        return new Integer (lNumber.intValue () + rNumber.intValue ());
    }
    
    @Override
    public final Number floatDivide (final Value <?> value) 
    {
        if (!(value instanceof Number))
            throw new OperationNotSupportedException (Operator.FLOAT_DIVISION, value.getClass ());
            
        java.lang.Number lNumber = this.internal,
                         rNumber = ((Number) value).internal;
               
        return new Float (lNumber.doubleValue () / rNumber.doubleValue ());
    }
    
    @Override
    public final Number integerDivide (final Value <?> value) 
    { 
        try
            { return new Integer (floatDivide (value).internal.intValue ()); }
            
        catch (final OperationNotSupportedException ex)
            { throw new OperationNotSupportedException (Operator.INTEGER_DIVISION, value.getClass ()); }
    }
                 
    @Override
    public final Number modulo (final Value <?> value) 
    {
        if (!(value instanceof Number))
            throw new OperationNotSupportedException (Operator.MODULO, value.getClass ());
            
        java.lang.Number lNumber = this.internal,
                         rNumber = ((Number) value).internal;
               
        if (this instanceof Float)
        {
            if (value instanceof Float)
                return new Float (lNumber.doubleValue () % rNumber.doubleValue ());
                
            else
                return new Float (lNumber.doubleValue () % rNumber.intValue ());
        }
        
        else if (value instanceof Float)
            return new Float (lNumber.intValue () % rNumber.doubleValue ());
            
        return new Integer (lNumber.intValue () % rNumber.intValue ());
    }
    
    @Override
    public final Number multiply (final Value <?> value) 
    {
        if (!(value instanceof Number))
            throw new OperationNotSupportedException (Operator.MULTIPLY, value.getClass ());
            
        java.lang.Number lNumber = this.internal,
                         rNumber = ((Number) value).internal;
               
        if (this instanceof Float)
        {
            if (value instanceof Float)
                return new Float (lNumber.doubleValue () * rNumber.doubleValue ());
                
            else
                return new Float (lNumber.doubleValue () * rNumber.intValue ());
        }
        
        else if (value instanceof Float)
            return new Float (lNumber.intValue () * rNumber.doubleValue ());
            
        return new Integer (lNumber.intValue () * rNumber.intValue ());
    }
    
    @Override
    public final Number negate ()
        { return this instanceof Float ? new Float (-internal.doubleValue ()) : new Integer (-internal.intValue ()); }
    
    @Override
    public final Number pow (final Value <?> value)
    {
        if (!(value instanceof Number))
            throw new OperationNotSupportedException (Operator.POW, value.getClass ());
            
        java.lang.Number lNumber = this.internal,
                         rNumber = ((Number) value).internal;
               
        if (this instanceof Float)
        {
            if (value instanceof Float)
                return new Float ((double) Math.pow (lNumber.doubleValue (), rNumber.doubleValue ()));
                
            else
                return new Float ((double) Math.pow (lNumber.doubleValue (), rNumber.intValue ()));
        }
        
        else if (value instanceof Float)
            return new Float ((double) Math.pow (lNumber.intValue (), rNumber.doubleValue ()));
            
        return new Integer ((int) Math.pow (lNumber.intValue (), rNumber.intValue ()));
    }
        
    @Override
    public final Number subtract (final Value <?> value)
    {
        try
            { return add (value.negate ()); }
        
        catch (final OperationNotSupportedException ex)
            { throw new OperationNotSupportedException (Operator.SUBTRACTION, value.getClass ()); }
    }
    
    /**
     * Attempts to parse the given {@link String} value into either a {@link Float} or an
     *  {@link Integer} instance.
     * 
     * @param value - The String value to attempt to parse
     * 
     * @return The given value parsed as either a Float or an Integer instance
     * @throws UnsupportedOperationException if the given value could not be parsed
     */
    public static final Number parse (final String value) 
    {
        try
            { return new Integer (java.lang.Integer.parseInt (value)); }
            
        catch (final NumberFormatException ignore)
        {
            // A floating point number cannot be parsed as an integer, so try float
            try
                { return new Float (java.lang.Double.parseDouble (value)); }
                
            catch (final NumberFormatException ex)
                { throw new UnsupportedOperationException (); }
        }
    }
}
