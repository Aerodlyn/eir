package io.gitlab.aerodlyn.eir.Types;

/**
 * Represents the {@link Float} type of Eir, which stores floating point values internally.
 * ! To figure out, rounding issues
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.28
 */
public final class Float extends Number
{
    /**
     * Constructs a new {@link Float} object with the given initial value.
     *  NOTE: Floats in Eir are stored as doubles, as there is no separate concept of float 
     *          vs. double
     * 
     * @param initial - The initial value to set this instance's internal value
     */ 
    public Float (final java.lang.Double initial)
        { super (initial); }
        
    @Override
    public String toString ()
        { return internal != null ? String.valueOf (internal.floatValue ()) : "undefined"; }
}
