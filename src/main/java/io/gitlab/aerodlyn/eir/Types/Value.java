package io.gitlab.aerodlyn.eir.Types;

import io.gitlab.aerodlyn.eir.Interfaces.Operations;

import lombok.Getter;

/**
 * Represents the basic Eir object that all other values extend. Contains a Java {@link Object}
 *  instance that represents the internal value stored, such as a number.
 * 
 * @param <T> - The internal value that is stored within the object (such as Float)
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.22
 */
public abstract class Value <T> implements Operations
{
    @Getter
    protected T internal;
}
