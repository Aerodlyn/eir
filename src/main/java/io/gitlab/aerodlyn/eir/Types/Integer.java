package io.gitlab.aerodlyn.eir.Types;

/**
 * Represents the {@link Integer} type of Eir, which stores non-floating point values internally.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.28
 */
public final class Integer extends Number
{
    /**
     * Constructs a new {@link Integer} object with the given initial value.
     * 
     * @param initial - The initial value to set this instance's internal value
     */   
    public Integer (final java.lang.Integer initial)
        { super (initial); }
        
    @Override
    public String toString ()
        { return internal != null ? String.valueOf (internal.intValue ()) : "undefined"; }
}
