package io.gitlab.aerodlyn.eir.Types;

import io.gitlab.aerodlyn.eir.Exceptions.OperationNotSupportedException;
import io.gitlab.aerodlyn.eir.Utils.Operator;

/**
 * Represents the {@link Boolean} type of Eir, which stores truthy/falsy values internally.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.28
 */
public final class Boolean extends Value <java.lang.Boolean>
{
    public static final Boolean FALSE   = new Boolean (false),
                                TRUE    = new Boolean (true);
    
    /**
     * Constructs a new {@link Boolean} object with the given initial value.
     * 
     * @param initial - The initial value to set this instance's internal value
     */
    public Boolean (final java.lang.Boolean initial)
        { internal = initial; }
        
    @Override
    public boolean equals (final Object obj)
    {
        if (obj instanceof Boolean)
            return internal.equals (((Boolean) obj).internal);
            
        return false;
    }
        
	@Override
    public Boolean absolute () 
        { throw new OperationNotSupportedException (Operator.ABSOLUTE, getClass ()); }

	@Override
    public Boolean add (final Value <?> value) 
        { throw new OperationNotSupportedException (Operator.ADDITION, getClass ()); }

	@Override
    public Boolean floatDivide (final Value <?> value) 
        { throw new OperationNotSupportedException (Operator.FLOAT_DIVISION, getClass ()); }

	@Override
    public Boolean integerDivide (final Value <?> value) 
        { throw new OperationNotSupportedException (Operator.INTEGER_DIVISION, getClass ()); }

    @Override
    public Boolean logicalNot () 
        { return new Boolean (!internal.booleanValue ()); }

	@Override
    public Boolean modulo (final Value <?> value) 
        { throw new OperationNotSupportedException (Operator.MODULO, getClass ()); }

	@Override
    public Boolean multiply (final Value <?> value) 
        { throw new OperationNotSupportedException (Operator.MULTIPLY, getClass ()); }

	@Override
    public Boolean negate () 
        { throw new OperationNotSupportedException (Operator.NEGATE, getClass ()); }

	@Override
    public Boolean pow (final Value <?> value) 
        { throw new OperationNotSupportedException (Operator.POW, getClass ()); }

	@Override
    public Boolean subtract (final Value <?> value) 
        { throw new OperationNotSupportedException (Operator.BITWISE_NOT, getClass ()); }
        
    @Override
    public Integer bitwiseNot ()
        { return new Integer (equals (TRUE) ? ~1 : ~0); }
        
    @Override
    public String toString ()
        { return internal != null ? String.valueOf (internal) : "undefined"; }
    
    /**
     * Attempts to parse the given {@link String} value into a {@link Boolean} instance.
     * 
     * @param value - The String value to attempt to parse
     * 
     * @return The given value parsed as a Boolean instance
     */
    public static final Boolean parse (final String value) 
        { return new Boolean (java.lang.Boolean.parseBoolean (value)); }
}
