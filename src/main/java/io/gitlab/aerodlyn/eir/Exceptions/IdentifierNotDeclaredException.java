package io.gitlab.aerodlyn.eir.Exceptions;

/**
 * Exception that details what identifier was not declared.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.26
 */
public final class IdentifierNotDeclaredException extends RuntimeException
{
    private static final long serialVersionUID = 870476729714775835L;

	/**
     * Constructs a new {@link IdentifierNotDeclaredException} instance detailing the identifier
     *  that was not declared.
     * 
     * @param identifier - The identifier that was not declared
     */
    public IdentifierNotDeclaredException (final String identifier)
        { super (String.format ("Identifier '%s' cannot be resolved, it's not declared", identifier)); }
}
