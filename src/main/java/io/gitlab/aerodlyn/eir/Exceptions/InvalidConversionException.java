package io.gitlab.aerodlyn.eir.Exceptions;

/**
 * Exception that details that an invalid assignment was attempted.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.10.01
 */
public final class InvalidConversionException extends RuntimeException
{
    private static final long serialVersionUID = 2949785651643445479L;

    /**
     * Constructs a new {@link InvalidConversionException} instance detailing the
     * type that cannot be converted to what type.
     * 
     * @param assignee - The {@link Class} that was attempted to be assigned to
     * @param assigned - The Class that an expression evaluated to
     */
	public InvalidConversionException (final Class <?> assignee, final Class <?> assigned)
    {
        super (String.format 
        (
            "The type '%s' cannot be converted to a variable of type '%s'",
            assigned.toString ().substring (assigned.toString ().lastIndexOf (".") + 1),
            assignee.toString ().substring (assignee.toString ().lastIndexOf (".") + 1)
        ));
    }
}
