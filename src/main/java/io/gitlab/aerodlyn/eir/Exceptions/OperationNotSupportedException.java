package io.gitlab.aerodlyn.eir.Exceptions;

import io.gitlab.aerodlyn.eir.Utils.Operator;

/**
 * Exception that details what operation is not supported for what Eir type.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.25
 */
public final class OperationNotSupportedException extends RuntimeException
{
	private static final long serialVersionUID = 4284733802930608515L;

    /**
     * Constructs a new {@link OperationNotSupportedException} instance detailing the Eir type
     *  that does not support the given operator.
     * 
     * @param operator  - The {@link Operator} that the given Eir type does not support
     * @param operand   - The Eir type that does not support the given operator
     */
	public OperationNotSupportedException (final Operator operator, final Class <?> operand)
    { 
        super (String.format 
        (
            "The %s '%s' operator is not supported for the '%s' type",
            operator.position.toLowerCase (),
            operator.token,
            operand.toString ().substring (operand.toString ().lastIndexOf (".") + 1)
        ));
    }
}
