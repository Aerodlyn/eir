package io.gitlab.aerodlyn.eir.Utils;

import java.util.HashMap;

import io.gitlab.aerodlyn.eir.Exceptions.IdentifierNotDeclaredException;

/**
 * Defines the values available to a given section of Eir code.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.0.27
 */
public final class Scope
{
    private HashMap <String, Variable>  variables;
    private Scope                       parent;
    
    /**
     * Constructs a new {@link Scope} instance with no parent.
     */
    public Scope ()
        { this (null); }
        
    /**
     * Constructs a new {@link Scope} instance with the given parent.
     * 
     * @param parent - The parent Scope instance to reference if a value cannot be resolved in
     *                  this scope
     */
    public Scope (final Scope parent)
    {
        variables = new HashMap <> ();
        this.parent = parent;
    }
    
    /**
     * Declares and stores the given variable and can later be referenced with the given identifier.
     * 
     * @param identifier    - The {@link String} that should be used as a reference for the variable
     * @param value         - The {@link Variable} instance to store
     */
    public void declare (final String identifier, final Variable value)
        { variables.put (identifier, value); }
    
    /**
     * Returns the {@link HashMap} that contains stored {@link Variable} instances.
     *  Should not be used except for testing.
     * 
     * @return The HashMap instance that contains the Value instances stored in this {@link Scope}
     */
    public HashMap <String, Variable> getRawValues ()
        { return variables; }
    
    /**
     * Attempts to find and return the {@link Variable} instance that was declared with the given
     *  identifier.
     * 
     * @param identifier - The {@link String} that represents the Variable instance being requested
     * 
     * @return The Value instance that was declared using the given identifier
     * @throws IdentifierNotDeclaredException if no Variable instance can be found with the given
     *          identifier
     */
    public Variable resolve (final String identifier)
    {
        if (!variables.containsKey (identifier))
        {
            if (parent == null)
                throw new IdentifierNotDeclaredException (identifier);
                
            return parent.resolve (identifier);
        }
            
        return variables.get (identifier);
    }
}
