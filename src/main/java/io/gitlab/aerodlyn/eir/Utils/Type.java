package io.gitlab.aerodlyn.eir.Utils;

import io.gitlab.aerodlyn.eir.Types.Boolean;
import io.gitlab.aerodlyn.eir.Types.Float;
import io.gitlab.aerodlyn.eir.Types.Integer;
import io.gitlab.aerodlyn.eir.Types.Value;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

/**
 * Represents the built-in types of Eir, and their association with keywords.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.10.01
 */
@RequiredArgsConstructor (access = AccessLevel.PRIVATE)
public enum Type
{
    BOOLEAN ("bool"),
    FLOAT ("float"),
    INTEGER ("int");

    public final String keyword;

    /**
     * Returns the {@link Type} that is associated with the given keyword.
     * 
     * @param keyword - The keyword to find the Type that is associated to it
     * 
     * @return The Type that is associated with the given keyword
     * @throws IllegalArgumentException if the given keyword is not associated to any built-in type
     */
    public static Type of (final String keyword)
    {
        for (final Type type : Type.values ())
        {
            if (type.keyword.equalsIgnoreCase (keyword))
                return type;
        }

        throw new IllegalArgumentException ();
    }

    /**
     * Returns the {@link Type} of the given {@link Value} instance.
     * 
     * @param value - The Value instance to determine the type of
     * 
     * @return The Type of the given Value instance
     * @throws UnsupportedOperationException if null is given or some invalid Value instance 
     *          is given
     */
    public static Type of (final Value <?> value)
    {
        if (value == null)
            throw new UnsupportedOperationException ();

        else if (value instanceof Boolean)
            return BOOLEAN;

        else if (value instanceof Float)
            return FLOAT;

        else if (value instanceof Integer)
            return INTEGER;

        else
            throw new UnsupportedOperationException ();
    }
}
