package io.gitlab.aerodlyn.eir.Utils;

import io.gitlab.aerodlyn.eir.Exceptions.InvalidConversionException;
import io.gitlab.aerodlyn.eir.Types.Boolean;
import io.gitlab.aerodlyn.eir.Types.Float;
import io.gitlab.aerodlyn.eir.Types.Integer;
import io.gitlab.aerodlyn.eir.Types.Number;
import io.gitlab.aerodlyn.eir.Types.Value;

// ? Should there be a separate method for explicit conversions, such as int -> bool
/**
 * Utilities related to value conversions.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.10.01
 */
public final class Conversion
{
    /**
     * Checks if the given {@link Value} instance can be converted to the given {@link Type}.
     *  Returns the result of the conversion if successful, otherwise throws an exception. Note
     *  that a null value can be "converted" to any type.
     * 
     * @param type  - The type to attempt to convert to
     * @param value - The value to attempt to convert
     * 
     * @return The converted value if successful
     * @throws InvalidConversionException if the conversion to the given type cannot take place
     */
    public static Value <?> check (final Type type, final Value <?> value)
    {
        if (value == null)
            return value;
        
        switch (type)
        {
            case BOOLEAN:
                if (!(value instanceof Boolean))
                    throw new InvalidConversionException (Boolean.class, value.getClass ());
                
                return value;
                
            case FLOAT:                    
            case INTEGER:
                if (!(value instanceof Number))
                    throw new InvalidConversionException (type.equals (Type.FLOAT) ? Float.class : Integer.class, value.getClass ());
                    
                return type.equals (Type.FLOAT) ? ((Number) value).floatDivide (new Float (1d)) : ((Number) value.integerDivide (new Integer (1)));
                
            default:
                throw new UnsupportedOperationException ();
        }
    }
}
