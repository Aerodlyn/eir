package io.gitlab.aerodlyn.eir.Utils;

import io.gitlab.aerodlyn.eir.Types.Value;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents a basic variable in Eir, meaning that the value stored can change as well as type.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.10.01
 */
@AllArgsConstructor (access = AccessLevel.PUBLIC)
public class Variable
{
    /**
     * Represents a const variable in Eir, meaning that neither the value stored or the variable's
     *  type can change.
     * 
     * @author  Patrick Jahnig (Aerodlyn)
     * @version 2019.10.01
     */
    public static final class ConstVariable extends TypedVariable
    {
        /**
         * Creates a new {@link ConstVariable} instance with the given {@link Type} and constant 
         *  value (not allowed to be null).
         * 
         * @param type      - The Type that the variable was declared to be
         * @param stored    - The constant value to store in the variable, null not allowed
         * 
         * @throws UnsupportedOperationException if the given value is null
         */
        public ConstVariable (final Type type, final Value <?> stored)
        {
            super (type, stored);

            if (stored == null)
                throw new UnsupportedOperationException ();
        }

        @Override
        public void setStored (final Value <?> value)
            { throw new UnsupportedOperationException (); }
    }

    /**
     * Represents a typed variable in Eir, meaning that the value stored can change but the
     *  variable's type cannot.
     * 
     * @author  Patrick Jahnig (Aerodlyn)
     * @version 2019.10.01
     */
    public static class TypedVariable extends Variable
    {
        private Type type;
        
        /**
         * Creates a new {@link TypedVariable} instance with the given {@link Type} and initial 
         *  value (allowed to be null).
         * 
         * @param type      - The Type that the variable was declared to be
         * @param stored    - The initial value to store in the variable, null allowed
         */
        public TypedVariable (final Type type, final Value <?> stored)
        {
            super (Conversion.check (type, stored));
            this.type = type;
        }
            
        @Override
        public void setStored (final Value <?> value)
            { super.setStored (Conversion.check (type, value)); }
    }
    
    @Getter @Setter
    protected Value <?> stored;
}
