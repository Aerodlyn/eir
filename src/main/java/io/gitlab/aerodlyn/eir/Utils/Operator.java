package io.gitlab.aerodlyn.eir.Utils;

import lombok.RequiredArgsConstructor;

/**
 * Represents an Eir type that details the position and token of the operator.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.25
 */
@RequiredArgsConstructor
public enum Operator
{
    ABSOLUTE ("PREFIX", "+"),
    ADDITION ("INFIX", "+"),
    BITWISE_NOT ("PREFIX", "~"),
    FLOAT_DIVISION ("INFIX", "/"),
    INTEGER_DIVISION ("INFIX", "//"),
    LOGICAL_NOT ("PREFIX", "!"),
    MODULO ("INFIX", "%"),
    MULTIPLY ("INFIX", "*"),
    NEGATE ("PREFIX", "-"),
    POW ("INFIX", "**"),
    SUBTRACTION ("INFIX", "-");
    
    public final String position,
                        token;
}
