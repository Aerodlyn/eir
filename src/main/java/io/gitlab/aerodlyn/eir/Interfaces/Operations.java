package io.gitlab.aerodlyn.eir.Interfaces;

import io.gitlab.aerodlyn.eir.Types.Value;

/**
 * Defines all valid operations for all Eir objects.
 * 
 * @param <T> - The internal value that is stored within the object (such as Float)
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.24
 */
public interface Operations
{
    /**
     * The method that is called when the prefix '+' operator is used. The value used as the only
     *  operand is the {@link Value} instance that this method was called from. Example: +a
     * 
     * @return A new Value instance that is the result of the absolute operation
     */
    public Value <?> absolute ();
    
    /**
     * The method that is called when the infix '+' operator is used. The value used as the left
     *  operand is the {@link Value} instance that this method was called from, while the right
     *  operand is the Value instance given as the parameter. Example: a + b
     * 
     * @param value - The Value instance to use as the right operand
     * 
     * @return A new Value instance that is the result of the addition operation
     */
    public Value <?> add (final Value <?> value);
    
    /**
     * The method that is called when the prefix '~' operator is used. The value used as the only
     *  operand is the {@link Value} instance that this method was called from. Example: ~a
     * 
     * @return A new Value instance that is the result of the bitwise NOT operation
     */
    public Value <?> bitwiseNot ();
    
    /**
     * The method that is called when the infix '/' operator is used. The value used as the left
     *  operand is the {@link Value} instance that this method was called from, while the right
     *  operand is the Value instance given as the parameter. Example: a / b
     * 
     * @param value - The Value instance to use as the right operand
     * 
     * @return A new Value instance that is the result of the float division operation
     */
    public Value <?> floatDivide (final Value <?> value);
    
    /**
     * The method that is called when the infix '//' operator is used. The value used as the left
     *  operand is the {@link Value} instance that this method was called from, while the right
     *  operand is the Value instance given as the parameter. Example: a // b
     * 
     * @param value - The Value instance to use as the right operand
     * 
     * @return A new Value instance that is the result of the integer division operation
     */
    public Value <?> integerDivide (final Value <?> value);
    
    /**
     * The method that is called when the prefix '!' operator is used. The value used as the only
     *  operand is the {@link Value} instance that this method was called from. Example: !a
     * 
     * @return A new Value instance that is the result of the logical NOT operation
     */
    public Value <?> logicalNot ();
    
    /**
     * The method that is called when the infix '%' operator is used. The value used as the left
     *  operand is the {@link Value} instance that this method was called from, while the right
     *  operand is the Value instance given as the parameter. Example: a % b
     * 
     * @param value - The Value instance to use as the right operand
     * 
     * @return A new Value instance that is the result of the modulo operation
     */
    public Value <?> modulo (final Value <?> value);
    
    /**
     * The method that is called when the infix '*' operator is used. The value used as the left
     *  operand is the {@link Value} instance that this method was called from, while the right
     *  operand is the Value instance given as the parameter. Example: a * b
     * 
     * @param value - The Value instance to use as the right operand
     * 
     * @return A new Value instance that is the result of the multiplication operation
     */
    public Value <?> multiply (final Value <?> value);
    
    /**
     * The method that is called when the prefix '-' operator is used. The value used as the only
     *  operand is the {@link Value} instance that this method was called from. Example: -a
     * 
     * @return A new Value instance that is the result of the negate operation
     */
    public Value <?> negate ();
    
    /**
     * The method that is called when the infix '**' operator is used. The value used as the left
     *  operand is the {@link Value} instance that this method was called from, while the right
     *  operand is the Value instance given as the parameter. Example: a ** b
     * 
     * @param value - The Value instance to use as the right operand
     * 
     * @return A new Value instance that is the result of the power operation
     */
    public Value <?> pow (final Value <?> value);
    
    /**
     * The method that is called when the infix '-' operator is used. The value used as the left
     *  operand is the {@link Value} instance that this method was called from, while the right
     *  operand is the Value instance given as the parameter. Example: a - b
     * 
     * @param value - The Value instance to use as the right operand
     * 
     * @return A new Value instance that is the result of the subtraction operation
     */
    public Value <?> subtract (final Value <?> value);
}
