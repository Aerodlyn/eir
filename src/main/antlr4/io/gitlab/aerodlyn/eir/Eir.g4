grammar Eir;

fragment A              : [aA];
fragment B              : [bB];
fragment C              : [cC];
fragment D              : [dD];
fragment E              : [eE];
fragment F              : [fF];
fragment G              : [gG];
fragment H              : [hH];
fragment I              : [iI];
fragment J              : [jJ];
fragment K              : [kK];
fragment L              : [lL];
fragment M              : [mM];
fragment N              : [nN];
fragment O              : [oO];
fragment P              : [pP];
fragment Q              : [qQ];
fragment R              : [rR];
fragment S              : [sS];
fragment T              : [tT];
fragment U              : [uU];
fragment V              : [vV];
fragment W              : [wW];
fragment X              : [xX];
fragment Y              : [yY];
fragment Z              : [zZ];

/* Symbols */
fragment DOT            : '.';

/* Type Literals */
// Bool
BOOL_LITERAL            : TRUE 
                        | FALSE
                        ;

// Number (Integer and Float)
fragment ZERO           : '0';
fragment NON_ZERO_DIGIT : [1-9];
fragment DIGIT          : ZERO | NON_ZERO_DIGIT;

NUMBER_LITERAL          : INTEGER_LITERAL 
                        | FLOAT_LITERAL
                        ;

INTEGER_LITERAL         : ZERO
                        | NON_ZERO_DIGIT DIGIT*
                        ;
                        
FLOAT_LITERAL           : INTEGER_LITERAL DOT DIGIT+;

/* Type collection */
TYPE                    : BOOL
                        | FLOAT
                        | INT
                        ;

/* Keywords */
BEGIN                   : B E G I N;
BOOL                    : B O O L;
CONST                   : C O N S T;
END                     : E N D;
FALSE                   : F A L S E;
FLOAT                   : F L O A T;
INT                     : I N T;
LET                     : L E T;
TRUE                    : T R U E;

/* Identifiers */
fragment ALPHA          : [a-zA-Z];
fragment ALPHA_NUMERIC  : ALPHA | DIGIT;

IDENTIFIER              : ALPHA ALPHA_NUMERIC*; 

/* Entry */
program                 : statement
                        | expression
                        ;

/* Expressions */
expression              : op = ('+' | '-' | '!' | '~') expression               # unaryExpression
                        | expression op = '**' expression                       # powExpression
                        | expression op = ('*' | '/' | '//' | '%') expression   # multExpression
                        | expression op = ('+' | '-') expression                # addExpression
                        | '(' expression ')'                                    # groupedExpression
                        | BOOL_LITERAL                                          # boolExpression
                        | NUMBER_LITERAL                                        # numberExpression
                        | IDENTIFIER                                            # identifierExpression
                        ;
                        
/* Statements */
statement               : LET IDENTIFIER ('<-' expression)?                     # letStatement
                        | TYPE IDENTIFIER ('<-' expression)?                    # typedStatement
                        | CONST TYPE? IDENTIFIER '<-' expression                # constStatement
                        | IDENTIFIER '<-' expression                            # reassignmentStatement
                        ;

/* Ignore */
WS                      : [ \n\t\r]+ -> skip;
