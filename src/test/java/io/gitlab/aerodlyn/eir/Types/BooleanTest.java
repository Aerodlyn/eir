package io.gitlab.aerodlyn.eir.Types;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import lombok.extern.flogger.Flogger;

/**
 * Tests the functionality of {@link Boolean} methods.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.28
 */
@Flogger
public class BooleanTest
{
    @Test
    public void testBitwiseNot ()
    {   
        log.atInfo ().log ("Checking bitwise not value of a literal boolean value that is truthy");
        
        Integer trueResult = Boolean.TRUE.bitwiseNot ();
        assertEquals (-2, trueResult.internal.intValue ());
        
        log.atInfo ().log ("Checking bitwise not value of a literal boolean value that is falsy");
        
        Integer falseResult = Boolean.FALSE.bitwiseNot ();
        assertEquals (-1, falseResult.internal.intValue ());
    }
    
    @Test
    public void testLogicalNot ()
    {
        log.atInfo ().log ("Checking logical not value of a literal boolean value that should be falsy");
        assertFalse (new Boolean (true).logicalNot ().internal.booleanValue ());
        
        log.atInfo ().log ("Checking logical not value of a literal boolean value that should be truthy");
        assertTrue (new Boolean (false).logicalNot ().internal.booleanValue ());
    }
    
    @Test
    public void testToString ()
    {
        log.atInfo ().log ("Checking String representation of a literal boolean values");
        
        assertEquals ("true", new Boolean (true).toString ());
        assertEquals ("false", new Boolean (false).toString ());
        assertEquals ("undefined", new Boolean (null).toString ());
    }
}
