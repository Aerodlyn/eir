package io.gitlab.aerodlyn.eir.Types;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import io.gitlab.aerodlyn.eir.Exceptions.OperationNotSupportedException;

import lombok.extern.flogger.Flogger;

/**
 * Tests the functionality of {@link Number} methods, testing the results of {@link Float} and
 *  {@link Integer} operations.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.28
 */
@Flogger
public class NumberTest
{
    @Test
    public void testAbsolute ()
    {
        log.atInfo ().log ("Checking absolute value of literal float value");
        
        Number floatResult = new Float (-5.5d).absolute ();
        assertTrue (floatResult instanceof Float);
        assertEquals (5.5d, floatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking absolute value of literal integer value");
        
        Number integerResult = new Integer (-5).absolute ();
        assertTrue (integerResult instanceof Integer);
        assertEquals (5, integerResult.internal.intValue ());
    }
    
    @Test
    public void testAdd ()
    {
        Float lFloat = new Float (5.5d),
              rFloat = new Float (2.75d);
              
        Integer lInteger = new Integer (5),
                rInteger = new Integer (11);
        
        log.atInfo ().log ("Checking added value of two literal float values");

        Number floatResult = lFloat.add (rFloat);
        assertTrue (floatResult instanceof Float);
        assertEquals (8.25d, floatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking added value of a literal float value and a literal integer value");
        
        Number floatIntegerResult = lFloat.add (rInteger);
        assertTrue (floatIntegerResult instanceof Float);
        assertEquals (16.5d, floatIntegerResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking added value of a literal integer value and a literal float value");
        
        Number integerFloatResult = lInteger.add (rFloat);
        assertTrue (integerFloatResult instanceof Float);
        assertEquals (7.75d, integerFloatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking added value of two literal integer values");
        
        Number integerResult = lInteger.add (rInteger);
        assertTrue (integerResult instanceof Integer);
        assertEquals (16, integerResult.internal.intValue ());
        
        log.atInfo ().log ("Checking that an exception is thrown when attempting to add a literal boolean value to literal float value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lFloat.add (Boolean.TRUE); }
        );
        
        log.atInfo ().log ("Checking that an exception is thrown when attempting to add a literal boolean value to literal integer value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lInteger.add (Boolean.TRUE); }
        );
    }
    
    @Test
    public void testBitwiseNot ()
    {
        log.atInfo ().log ("Checking bitwise value of literal float value");
        
        Integer floatResult = new Float (5.5d).bitwiseNot ();
        assertEquals (-6, floatResult.internal.intValue ());
        
        log.atInfo ().log ("Checking bitwise value of literal integer value");
        
        Integer integerResult = new Integer (5).bitwiseNot ();
        assertEquals (-6, integerResult.internal.intValue ());
    }
    
    @Test
    public void testFloatDivide ()
    {
        Float lFloat = new Float (5.5d),
              rFloat = new Float (2.75d);
              
        Integer lInteger = new Integer (5),
                rInteger = new Integer (11);
        
        log.atInfo ().log ("Checking float division value of two literal float values");

        Number floatResult = lFloat.floatDivide (rFloat);
        assertTrue (floatResult instanceof Float);
        assertEquals (2.0d, floatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking float division value of a literal float value and a literal integer value");
        
        Number floatIntegerResult = lFloat.floatDivide (rInteger);
        assertTrue (floatIntegerResult instanceof Float);
        assertEquals (0.5d, floatIntegerResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking float division value of a literal integer value and a literal float value");
        
        Number integerFloatResult = lInteger.floatDivide (rFloat);
        assertTrue (integerFloatResult instanceof Float);
        assertEquals (1.818d, integerFloatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking float division value of two literal integer values");
        
        Number integerResult = lInteger.floatDivide (rInteger);
        assertTrue (integerResult instanceof Float);
        assertEquals (0.455d, integerResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to float divide a literal float value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lFloat.floatDivide (Boolean.TRUE); } 
        );
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to float divide a literal integer value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lInteger.floatDivide (Boolean.TRUE); } 
        );
    }
    
    @Test
    public void testIntegerDivide ()
    {
        Float lFloat = new Float (5.5d),
              rFloat = new Float (2.75d);
              
        Integer lInteger = new Integer (5),
                rInteger = new Integer (11);
                
        log.atInfo ().log ("Checking integer division value of two literal float values");
        
        Number floatResult = lFloat.integerDivide (rFloat);
        assertTrue (floatResult instanceof Integer);
        assertEquals (2, floatResult.internal.intValue ());
        
        log.atInfo ().log ("Checking integer division value of a literal float value and a literal integer value");
        
        Number floatIntegerResult = lFloat.integerDivide (rInteger);
        assertTrue (floatIntegerResult instanceof Integer);
        assertEquals (0, floatIntegerResult.internal.intValue ());
        
        log.atInfo ().log ("Checking integer division value of a literal integer value and a literal float value");
        
        Number integerFloatResult = lInteger.integerDivide (rFloat);
        assertTrue (integerFloatResult instanceof Integer);
        assertEquals (1, integerFloatResult.internal.intValue ());
        
        log.atInfo ().log ("Checking integer division value of two literal integer values");
        
        Number integerResult = lInteger.integerDivide (rInteger);
        assertTrue (integerResult instanceof Integer);
        assertEquals (0, integerResult.internal.intValue ());
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to integer divide a literal float value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lFloat.integerDivide (Boolean.TRUE); }
        );
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to integer divide a literal integer value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lInteger.integerDivide (Boolean.TRUE); }
        );
    }
    
    @Test
    public void testLogicalNot ()
    {
        log.atInfo ().log ("Checking logical not value of a literal float value that is falsy");
        assertFalse (new Float (5.5d).logicalNot ().internal.booleanValue ());
        
        log.atInfo ().log ("Checking logical not value of a literal integer value that is falsy");
        assertFalse (new Integer (5).logicalNot ().internal.booleanValue ());
        
        log.atInfo ().log ("Checking logical not value of a literal float value that is truthy");
        assertTrue (new Float (0d).logicalNot ().internal.booleanValue ());
        
        log.atInfo ().log ("Checking logical not value of a literal integer value that is truthy");
        assertTrue (new Integer (0).logicalNot ().internal.booleanValue ());
    }
    
    @Test
    public void testModulo ()
    {
        Float lFloat = new Float (5.5d),
              rFloat = new Float (2.75d);
              
        Integer lInteger = new Integer (5),
                rInteger = new Integer (11);
                
        log.atInfo ().log ("Checking modulo value of two literal float values");
        
        Number floatResult = lFloat.modulo (rFloat);
        assertTrue (floatResult instanceof Float);
        assertEquals (0d, floatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking modulo value of a literal float value and a literal integer value");
        
        Number floatIntegerResult = lFloat.modulo (rInteger);
        assertTrue (floatIntegerResult instanceof Float);
        assertEquals (5.5d, floatIntegerResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking modulo value of a literal integer value and a literal float value");
        
        Number integerFloatResult = lInteger.modulo (rFloat);
        assertTrue (integerFloatResult instanceof Float);
        assertEquals (2.25d, integerFloatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking modulo value of two literal integer values");
        
        Number integerResult = lInteger.modulo (rInteger);
        assertTrue (integerResult instanceof Integer);
        assertEquals (5, integerResult.internal.intValue ());
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to modulo a literal float value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lFloat.modulo (Boolean.TRUE); }
        );
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to modulo a literal integer value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lInteger.modulo (Boolean.TRUE); }
        );
    }
    
    @Test
    public void testMultiply ()
    {
        Float lFloat = new Float (5.5d),
              rFloat = new Float (2.75d);
              
        Integer lInteger = new Integer (5),
                rInteger = new Integer (11);
        
        log.atInfo ().log ("Checking multiplication value of two literal float values");
        
        Number floatResult = lFloat.multiply (rFloat);
        assertTrue (floatResult instanceof Float);
        assertEquals (15.125d, floatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking multiplication value of a literal float value and a literal integer value");
        
        Number floatIntegerResult = lFloat.multiply (rInteger);
        assertTrue (floatIntegerResult instanceof Float);
        assertEquals (60.5d, floatIntegerResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking multiplication value of a literal integer value and a literal float value");
        
        Number integerFloatResult = lInteger.multiply (rFloat);
        assertTrue (integerFloatResult instanceof Float);
        assertEquals (13.75d, integerFloatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking multiplication value of two literal integer values");
        
        Number integerResult = lInteger.multiply (rInteger);
        assertTrue (integerResult instanceof Integer);
        assertEquals (55, integerResult.internal.intValue ());
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to multiply a literal float value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lFloat.multiply (Boolean.TRUE); }
        );
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to multiply a literal integer value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lInteger.multiply (Boolean.TRUE); }
        );
    }
    
    @Test
    public void testNegate ()
    {
        log.atInfo ().log ("Checking negation value of a literal float value");
        
        Number floatResult = new Float (5.5d).negate ();
        assertTrue (floatResult instanceof Float);
        assertEquals (-5.5d, floatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking negation value of a literal integer value");
        
        Number integerResult = new Integer (5).negate ();
        assertTrue (integerResult instanceof Integer);
        assertEquals (-5, integerResult.internal.intValue ());
    }
    
    @Test
    public void testPower ()
    {
        Float lFloat = new Float (5.5d),
              rFloat = new Float (2.75d);
              
        Integer lInteger = new Integer (5),
                rInteger = new Integer (11);
                
        log.atInfo ().log ("Checking power value of two literal float values");
        
        Number floatResult = lFloat.pow (rFloat);
        assertTrue (floatResult instanceof Float);
        assertEquals (108.642d, floatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking power value of a literal float value and a literal integer value");
        
        Number floatIntegerResult = lFloat.pow (rInteger);
        assertTrue (floatIntegerResult instanceof Float);
        assertEquals (139312339.166d, floatIntegerResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking power value of a literal integer value and a literal float value");
        
        Number integerFloatResult = lInteger.pow (rFloat);
        assertTrue (integerFloatResult instanceof Float);
        assertEquals (83.593d, integerFloatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking power value of two literal integer values");
        
        Number integerResult = lInteger.pow (rInteger);
        assertTrue (integerResult instanceof Integer);
        assertEquals (48828125, integerResult.internal.intValue ());
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to power a literal float value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lFloat.pow (Boolean.TRUE); }
        );
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to power a literal integer value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lInteger.pow (Boolean.TRUE); }
        );
    }
    
    @Test
    public void testParse ()
    {
        log.atInfo ().log ("Checking parsed value of a literal float value");
        
        Number f = Number.parse ("2.9208");
        assertEquals (Float.class, f.getClass ());
        assertEquals (2.9208d, f.getInternal ().doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking parsed value of a literal integer value");
        
        Number i = Number.parse ("8235");
        assertEquals (Integer.class, i.getClass ());
        assertEquals (8235, i.getInternal ().intValue ());
        
        log.atInfo ().log ("Checking that an exception is thrown when attempted to parse a non-number type into a number");
        
        assertThrows 
        (
            UnsupportedOperationException.class, 
            () -> { Number.parse ("1.5.2"); }
        );
    }
    
    @Test
    public void testSubtract ()
    {
        Float lFloat = new Float (5.5d),
              rFloat = new Float (2.75d);
              
        Integer lInteger = new Integer (5),
                rInteger = new Integer (11);
                
        log.atInfo ().log ("Checking subtraction value of two literal float values");
        
        Number floatResult = lFloat.subtract (rFloat);
        assertTrue (floatResult instanceof Float);
        assertEquals (2.75d, floatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking subtraction value of a literal float value and a literal integer value");
        
        Number floatIntegerResult = lFloat.subtract (rInteger);
        assertTrue (floatIntegerResult instanceof Float);
        assertEquals (-5.5d, floatIntegerResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking subtraction value of a literal integer value and a literal float value");
        
        Number integerFloatResult = lInteger.subtract (rFloat);
        assertTrue (integerFloatResult instanceof Float);
        assertEquals (2.25d, integerFloatResult.internal.doubleValue (), 0.001d);
        
        log.atInfo ().log ("Checking subtraction value of two literal integer values");
        
        Number integerResult = lInteger.subtract (rInteger);
        assertTrue (integerResult instanceof Integer);
        assertEquals (-6, integerResult.internal.intValue ());
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to subtract a literal float value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lFloat.subtract (Boolean.TRUE); }
        );
        
        log.atInfo ().log ("Checking that an exception gets thrown when attempting to subtract a literal integer value by a literal boolean value");
        
        assertThrows
        (
            OperationNotSupportedException.class,
            () -> { lInteger.subtract (Boolean.TRUE); }
        );
    }
    
    @Test
    public void testToString ()
    {
        log.atInfo ().log ("Checking String representation of a literal float value");
        assertEquals ("5.25", new Float (5.25d).toString ());
        assertEquals ("undefined", new Float (null).toString ());
        
        log.atInfo ().log ("Checking String representation of a literal integer value");
        assertEquals ("437", new Integer (437).toString ());
        assertEquals ("undefined", new Integer (null).toString ());
    }
}
