package io.gitlab.aerodlyn.eir.Utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import io.gitlab.aerodlyn.eir.Exceptions.IdentifierNotDeclaredException;
import io.gitlab.aerodlyn.eir.Types.Integer;
import lombok.extern.flogger.Flogger;

/**
 * Tests the {@link Scope} class.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.28
 */
@Flogger
@TestMethodOrder (OrderAnnotation.class)
public class ScopeTest
{
    private Scope scope;
    
    @BeforeEach
    public void setUp ()
        { scope = new Scope (); }
    
    @Test
    @Order (1)
    public void testDeclare ()
    {
        log.atInfo ().log ("Checking that scope is initially empty");
        assertEquals (0, scope.getRawValues ().size ());
        
        log.atInfo ().log ("Checking that a new value can be declared and stored");
        
        scope.declare ("a", new Variable (new Integer (5)));
        
        assertEquals (1, scope.getRawValues ().size ());
        assertEquals (5, ((Number) scope.getRawValues ().get ("a").getStored ().getInternal ()).intValue ());
        
        log.atInfo ().log ("Checking that a new value can shadow another");
        
        scope.declare ("a", new Variable (new Integer (2)));
        
        assertEquals (1, scope.getRawValues ().size ());
        assertEquals (2, ((Number) scope.getRawValues ().get ("a").getStored ().getInternal ()).intValue ());
    }
        
    @Test
    @Order (2)
    public void testResolve ()
    {
        log.atInfo ().log ("Checking that a value can be retrieved");
        
        scope.declare ("a", new Variable (new Integer (5)));
        assertEquals (5, ((Number) scope.resolve ("a").getStored ().getInternal ()).intValue ());
        
        log.atInfo ().log ("Checking that an exception is thrown if the identifier was never declared");
        assertThrows
        (
            IdentifierNotDeclaredException.class, 
            () -> { scope.resolve ("b"); }
        );
        
        log.atInfo ().log ("Check that a value can be found in a parent scope");
        
        scope.declare ("b", new Variable (new Integer (2)));
        assertEquals (2, ((Number) new Scope (scope).resolve ("b").getStored ().getInternal ()).intValue ());
    }
}
