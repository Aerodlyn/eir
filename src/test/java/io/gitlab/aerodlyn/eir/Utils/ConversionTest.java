package io.gitlab.aerodlyn.eir.Utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import io.gitlab.aerodlyn.eir.Exceptions.InvalidConversionException;
import io.gitlab.aerodlyn.eir.Types.Boolean;
import io.gitlab.aerodlyn.eir.Types.Float;
import io.gitlab.aerodlyn.eir.Types.Integer;
import io.gitlab.aerodlyn.eir.Types.Value;

import lombok.extern.flogger.Flogger;

/**
 * Tests the {@link Conversion} class.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.10.01
 */
@Flogger
public class ConversionTest
{
    @Test
    public void testCheck ()
    {
        log.atInfo ().log ("Testing static check method converts valid types correctly");
        
        Value <?> value = Conversion.check (Type.BOOLEAN, new Boolean (true));

        assertNotNull (value);
        assertTrue (value instanceof Boolean);
        assertEquals (Boolean.TRUE, (Boolean) value);
        
        value = Conversion.check (Type.FLOAT, new Float (2.5d));

        assertNotNull (value);
        assertTrue (value instanceof Float);
        assertEquals (2.5d, ((Float) value).getInternal ().doubleValue (), 0.001d);
        
        value = Conversion.check (Type.INTEGER, new Integer (4));

        assertNotNull (value);
        assertTrue (value instanceof Integer);
        assertEquals (4, ((Integer) value).getInternal ().intValue ());
        
        value = Conversion.check (Type.INTEGER, new Float (2.5d));

        assertNotNull (value);
        assertTrue (value instanceof Integer);
        assertEquals (2, ((Integer) value).getInternal ().intValue ());

        log.atInfo ().log ("Testing static check method throws an exception if a conversion cannot occur");

        assertThrows
        (
            InvalidConversionException.class,
            () -> { Conversion.check (Type.BOOLEAN, new Integer (5)); }
        );
    }
}
