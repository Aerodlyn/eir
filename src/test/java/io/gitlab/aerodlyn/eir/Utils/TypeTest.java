package io.gitlab.aerodlyn.eir.Utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import io.gitlab.aerodlyn.eir.Types.Boolean;
import io.gitlab.aerodlyn.eir.Types.Float;
import io.gitlab.aerodlyn.eir.Types.Integer;
import io.gitlab.aerodlyn.eir.Types.Value;

import lombok.extern.flogger.Flogger;

/**
 * Tests the {@link Type} class.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.10.01
 */
@Flogger
public class TypeTest
{
    @Test
    public void testOfString ()
    {
        log.atInfo ().log ("Testing static constructor (String version) of Type class");
        
        assertEquals (Type.BOOLEAN, Type.of ("bool"));
        assertEquals (Type.FLOAT, Type.of ("float"));
        assertEquals (Type.INTEGER, Type.of ("int"));

        assertThrows
        (
            IllegalArgumentException.class,
            () -> { Type.of ("not a type"); }
        );
    }

    @Test
    public void testOfValue ()
    {
        log.atInfo ().log ("Testing static constructor (Value version) of Type class");
        
        assertEquals (Type.BOOLEAN, Type.of (new Boolean (true)));
        assertEquals (Type.FLOAT, Type.of (new Float (5.25d)));
        assertEquals (Type.INTEGER, Type.of (new Integer (3)));

        assertThrows
        (
            UnsupportedOperationException.class,
            () -> { Type.of ((Value <?>) null); }
        );
    }
}
