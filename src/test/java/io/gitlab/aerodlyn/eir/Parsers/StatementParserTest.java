package io.gitlab.aerodlyn.eir.Parsers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.gitlab.aerodlyn.eir.EirLexer;
import io.gitlab.aerodlyn.eir.EirParser;
import io.gitlab.aerodlyn.eir.Visitor;
import io.gitlab.aerodlyn.eir.Exceptions.InvalidConversionException;
import io.gitlab.aerodlyn.eir.Types.Boolean;
import io.gitlab.aerodlyn.eir.Types.Integer;
import io.gitlab.aerodlyn.eir.Types.Value;

import lombok.extern.flogger.Flogger;

/**
 * Tests the various statement related parsing and execution methods of Eir.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.10.01
 */
@Flogger
public class StatementParserTest
{
    private EirParser parser;
    private Visitor   visitor;
    
    @BeforeEach
    public void setUp ()
    {
        parser = new EirParser (null);
        visitor = new Visitor ();
    }

    @Test
    public void testEvaluateConstStatement ()
    {
        log.atInfo ().log ("Testing const let statement with no expression");

        setTokenStream ("const a");

        assertThrows
        (
            UnsupportedOperationException.class,
            () -> { visitor.visit (parser.program ()); }
        );

        log.atInfo ().log ("Testing const let statement with expression but no type");

        setTokenStream ("const a <- 5 * 3");

        Value <?> value = visitor.visit (parser.program ());

        assertNotNull (value);
        assertTrue (value instanceof Integer);
        assertEquals (15, ((Integer) value).getInternal ().intValue ());

        log.atInfo ().log ("Testing const let statement with expression and type (conversion)");

        setTokenStream ("const int a <- 2.5");

        value = visitor.visit (parser.program ());

        assertNotNull (value);
        assertTrue (value instanceof Integer);
        assertEquals (2, ((Integer) value).getInternal ().intValue ());
    }
    
    @Test
    public void testEvaluateLetStatement ()
    {
        log.atInfo ().log ("Testing let statement with no expression");
        
        setTokenStream ("let a");
        
        Value <?> value = visitor.visit (parser.program ());
        assertNull (value);
        
        log.atInfo ().log ("Testing let statement with expression");
        
        setTokenStream ("let a <- 5 * 3");
        
        value = visitor.visit (parser.program ());
        assertNotNull (value);
        assertTrue (value instanceof Integer);
        assertEquals (15, ((Integer) value).getInternal ().intValue ());
    }
    
    @Test
    public void testEvaluateReassignmentStatement ()
    {        
        log.atInfo ().log ("Testing that a let variable can be reassigned both value and type");
        
        setTokenStream ("let a <- true");
        
        Value <?> value = visitor.visit (parser.program ());
        assertNotNull (value);
        assertTrue (value instanceof Boolean);
        assertTrue (((Boolean) value).getInternal ().booleanValue ());
        
        setTokenStream ("a <- false");
        
        value = visitor.visit (parser.program ());
        assertNotNull (value);
        assertTrue (value instanceof Boolean);
        assertFalse (((Boolean) value).getInternal ().booleanValue ());
        
        setTokenStream ("a <- 5 * 3");
        
        value = visitor.visit (parser.program ());
        assertNotNull (value);
        assertTrue (value instanceof Integer);
        assertEquals (15, ((Integer) value).getInternal ().intValue ());
        
        log.atInfo ().log ("Testing that a typed let variable can be reassigned value but not type");
        
        setTokenStream ("bool a <- false");
        
        value = visitor.visit (parser.program ());
        assertNotNull (value);
        assertTrue (value instanceof Boolean);
        assertFalse (((Boolean) value).getInternal ().booleanValue ());
        
        setTokenStream ("a <- true");
        
        value = visitor.visit (parser.program ());
        assertNotNull (value);
        assertTrue (value instanceof Boolean);
        assertTrue (((Boolean) value).getInternal ().booleanValue ());
        
        setTokenStream ("a <- 15");
        
        assertThrows
        (
            InvalidConversionException.class,
            () -> { visitor.visit (parser.program ()); }
        );

        log.atInfo ().log ("Testing that a const let variable cannot be reassigned");

        setTokenStream ("const a <- 5");
        visitor.visit (parser.program ());

        setTokenStream ("a <- 2");

        assertThrows
        (
            UnsupportedOperationException.class,
            () -> { visitor.visit (parser.program ()); }
        );
    }
    
    @Test
    public void testEvaluateTypedStatement ()
    {
        log.atInfo ().log ("Testing typed let statement with no expression");
        
        setTokenStream ("int a");
        
        Value <?> value = visitor.visit (parser.program ());
        assertNull (value);
        
        log.atInfo ().log ("Testing typed let statement with expression");
        
        setTokenStream ("int a <- 5 * 3");
        
        value = visitor.visit (parser.program ());
        assertNotNull (value);
        assertTrue (value instanceof Integer);
        assertEquals (15, ((Integer) value).getInternal ().intValue ());
        
        log.atInfo ().log ("Testing typed let statement with expression that is cast-able");
        
        setTokenStream ("int a <- 5.25");
        
        value = visitor.visit (parser.program ());
        assertNotNull (value);
        assertTrue (value instanceof Integer);
        assertEquals (5, ((Integer) value).getInternal ().intValue ());
        
        log.atInfo ().log ("Testing typed let statement with conflicting expression");
        
        setTokenStream ("int a <- true");
        
        assertThrows
        (
            InvalidConversionException.class,
            () -> { visitor.visit (parser.program ()); }
        );
    }
    
    /**
     * Sets the {@link TokenStream} used by the Eir parser ({@link EirParser}) to use the given 
     *  {@link String} as input.
     * 
     * @param line - The String to use as input
     */
    private void setTokenStream (final String line)
        { parser.setTokenStream (new CommonTokenStream (new EirLexer (new ANTLRInputStream (line)))); }
}
