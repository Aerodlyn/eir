package io.gitlab.aerodlyn.eir.Parsers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import io.gitlab.aerodlyn.eir.EirLexer;
import io.gitlab.aerodlyn.eir.EirParser;
import io.gitlab.aerodlyn.eir.Visitor;
import io.gitlab.aerodlyn.eir.Types.Boolean;
import io.gitlab.aerodlyn.eir.Types.Float;
import io.gitlab.aerodlyn.eir.Types.Integer;
import io.gitlab.aerodlyn.eir.Types.Value;
import io.gitlab.aerodlyn.eir.Utils.Scope;
import io.gitlab.aerodlyn.eir.Utils.Variable;

import lombok.extern.flogger.Flogger;

/**
 * Tests the various expression related parsing and execution methods of Eir.
 * 
 * @author  Patrick Jahnig (Aerodlyn)
 * @version 2019.09.28
 */
@Flogger
public class ExpressionParserTest
{
    private EirParser parser;
    private Visitor   visitor;
    
    @BeforeEach
    public void setUp ()
    {
        parser = new EirParser (null);
        visitor = new Visitor ();
    }
    
    @ParameterizedTest
    @CsvSource
    ({
        "'5', '2 + 3'",
        "'235', '234 + 1'",
        "'0.5', '0.25 + 0.25'",
        "'36.9', '24.6 + 12.3'",
        "'12.2231', '12 + 0.2231'",
        "'5', '10 - 5'",
        "'235', '236 - 1'",
        "'0.5', '0.75 - 0.25'",
        "'36.9', '38.6 - 1.7'"
    })
    public void testEvaluateAddExpression (final String expected, final String expression)
    {
        log.atInfo ().log ("Testing capture and parsing of addition expression: %s", expression);
        
        setTokenStream (expression);
        assertEquals (expected, visitor.visit (parser.program ()).toString ());
    }
    
    @ParameterizedTest
    @ValueSource (strings = { "true", "TRUE", "tRuE" })
    public void testEvaluateBoolExpressionTrueValues (final String value)
    {
        log.atInfo ().log ("Testing capture and parsing of truthy literal boolean value: %s", value);
        
        setTokenStream (value);
        assertEquals (Boolean.TRUE, visitor.visit (parser.program ()));
    }
    
    @ParameterizedTest
    @ValueSource (strings = { "false", "FALSE", "fAlSe" })
    public void testEvaluateBoolExpressionFalseValues (final String value)
    {
        log.atInfo ().log ("Testing capture and parsing of falsy literal boolean value: %s", value);
           
        setTokenStream (value);
        assertEquals (Boolean.FALSE, visitor.visit (parser.program ()));
    }
    
    @ParameterizedTest
    @CsvSource
    ({
        "'5', '(5)'",
        "'4.4', '(4.4)'",
        "'6', '(2 + 4)'",
        "'1.256', '(1.05 + 0.206)'",
        "'8', '(2 * 4)'"
    })
    public void testEvaluateGroupedExpression (final String expected, final String expression)
    {
        log.atInfo ().log ("Testing capture and parsing of grouped expression: %s", expression);
        
        setTokenStream (expression);
        assertEquals (expected, visitor.visit (parser.program ()).toString ());
    }
    
    @Test
    public void testEvaluateIdentifierExpression ()
    {
        log.atInfo ().log ("Testing retrieval of stored value");
        
        Scope scope = new Scope ();
        scope.declare ("a", new Variable (new Integer (5)));
        
        Variable variable = scope.resolve ("a");
        
        assertTrue (variable.getStored () instanceof Integer);
        assertEquals (5, ((Integer) variable.getStored ()).getInternal ().intValue ());
    }
    
    @ParameterizedTest
    @CsvSource
    ({
        "'2.5', '5 / 2'",
        "'0.5', '8.92 / 17.84'",
        "'2', '5 // 2'",
        "'0', '8.92 // 17.84'",
        "'10', '5 * 2'",
        "'159.1328', '8.92 * 17.84'",
        "'1', '5 % 2'",
        "'8.92', '8.92 % 17.84'"
    })
    public void testEvaluateMultExpression (final String expected, final String expression)
    {
        log.atInfo ().log ("Testing capture and parsing of mult expression: %s", expression);
        
        setTokenStream (expression);
        assertEquals (expected, visitor.visit (parser.program ()).toString ());
    }
    
    @ParameterizedTest
    @CsvSource
    ({
        "5.236, '5.236'",
        "124.890346, '124.890346'",
        "22.22, '22.22'",
        "-1346.8327, '-1346.8327'" // Also tests negation
    })
    public void testEvaluateNumberExpressionFloatValues (final float expected, final String value)
    {
        log.atInfo ().log ("Testing capture and parsing of literal float value: %s", value);
        
        setTokenStream (value);

        Value <?> result = visitor.visit (parser.program ());
        
        assertTrue (result instanceof Float);
        assertEquals (expected, ((Float) result).getInternal ().floatValue (), 0.001f);
    }
    
    @ParameterizedTest
    @CsvSource
    ({
        "5, '5'",
        "124, '124'",
        "22, '22'",
        "-1346, '-1346'" // Also tests negation
    })
    public void testEvaluateNumberExpressionIntegerValues (final int expected, final String value)
    {
        log.atInfo ().log ("Testing capture and parsing of literal integer value: %s", value);
        
        setTokenStream (value);

        Value <?> result = visitor.visit (parser.program ());
        
        assertTrue (result instanceof Integer);
        assertEquals (expected, ((Integer) result).getInternal ().intValue ());
    }
    
    @ParameterizedTest
    @CsvSource
    ({
        "'25', '5 ** 2'",
        "'5.0', '25 ** 0.5'"
    })
    public void testEvaluatePowExpression (final String expected, final String expression)
    {
        log.atInfo ().log ("Testing capture and parsing of pow expression: %s", expression);
        
        setTokenStream (expression);
        assertEquals (expected, visitor.visit (parser.program ()).toString ());
    }
    
    @ParameterizedTest
    @CsvSource
    ({
        "'-1', '~false'",
        "'-2', '~true'",
        "'-8', '~7'",
        "'-8', '~7.56'",
        "'false', '!5'",
        "'true', '!0'",
        "'false', '!5.5'",
        "'true', '!0.0'",
        "'-10', '-(2 * 5)'",
        "'-5.25', '-(4.25 + 1)'",
        "'3', '+(-5 + 2)'",
        "'3.25', '+(-4.25 + 1)'"
    })
    public void testEvaluateUnaryExpression (final String expected, final String expression)
    {
        log.atInfo ().log ("Testing capture and parsing of unary expression: %s", expression);
        
        setTokenStream (expression);
        assertEquals (expected, visitor.visit (parser.program ()).toString ());
    }
    
    /**
     * Sets the {@link TokenStream} used by the Eir parser ({@link EirParser}) to use the given 
     *  {@link String} as input.
     * 
     * @param line - The String to use as input
     */
    private void setTokenStream (final String line)
        { parser.setTokenStream (new CommonTokenStream (new EirLexer (new ANTLRInputStream (line)))); }
}
