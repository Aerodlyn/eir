const Utils =
{
    /**
     * Counts the lines of code given, by counting the number of newlines.
     * 
     * @param {string} code - The code to count lines of
     * 
     * @returns {number} The number of lines in the given code
     */
    countLinesOfCode: code => code.split ('\n').length,

    /**
     * 
     * @param {string} code
     */
    createCodeBlock: (code = undefined) =>
    {
        const createCodeLines = () => code.split ("\n").map ((line, index) => `<tr><th>${ index + 1 }</th><td>${ line }</td></tr>`).join ("");
        return code ? `<table class="table is-pulled-right code-block rounded"><tbody>${ createCodeLines () }</tbody></table>` : "";
    },

    /**
     * Creates a new paragraph with the given text, and optional header and code snippet.
     * 
     * @param {string} text - The text of the paragraph
     * @param {{ title: string, code: string }} rest - The remaining optional parameters:
     *  code - The code snippet to include with the paragraph;
     *  table - The table to include with the paragraph
     *  title - The header to include with the paragraph
     * 
     * @returns {string} The HTML snippet that represents the paragraph constructed
     */
    createParagraph: (text, { code = undefined, table = undefined, title = undefined } = {}) => 
    {
        return `
            <div class = "is-clipped p-1">
                ${ title ? "<h6 class='title is-6'>" + title + "</h6>" : "" }
                ${ table ? table : "" }
                ${ Utils.createCodeBlock (code) }
                <div class="has-text-justified">${ text.split ("\n").map (line => `<p>${ line || "&nbsp;" }</p>`).join ("") }</div>
            </div>
        `;
    },

    /**
     * Creates a section with the given header and paragraphs.
     * 
     * @param {string} header - The header of the section
     * @param {Array <string>} paragraphs - The paragraphs that make up the section
     * 
     * @returns {string} The HTML snippet that represents the section constructed
     */
    createSection: (header, paragraphs) =>
    {
        return `
            <section class="section">
                <div class="container has-background-light rounded p-2">
                    <h4 class='title is-4'>${ header }</h4>
                    <div class="has-background-white rounded">
                        ${ paragraphs.join ("\n") }
                    </div>
                </div>
            </section>
        `;
    },

    /**
     * 
     * @param {Array <string>} headers 
     * @param {Array <Array <string>>} rows 
     * @param {string} float - The side to float the table on, valid values are "Left" or "Right" if defined
     */
    createTable: (headers, rows, float = undefined) =>
    {
        const createRow = columns => columns.map (cell => `<td>${ cell }</td>`).join ("");
        const floatSide = () => 
        {
            if (float)
            {
                float = float.toLowerCase ();
                return float === "left" ? "is-pulled-left" : (float === "right" ? "is-pulled-right" : "");
            }
            
            return "";
        };
        
        return `
            <table class="table ${ floatSide () } rounded">
                <thead>${ headers.map (header => `<th>${ header }</th>`).join ("") }</thead>
                <tbody>${ rows.map (columns => `<tr>${ createRow (columns) }</tr>`).join ("") }</tbody>
            </table>
        `;
    }
}

/* Content building */

const comments =
[
    p1 = () =>
    {
        const code = "# This is a single line comment\n" +
                     "#!\n" +
                     "&emsp;This is a multiline comment\n" +
                     "!#";
        const text = "Eir has two kinds of comments: single line and multiline. The code snippet to the right" +
                     " shows both. Comments can be nested, as single line comments are ignored until the end of" +
                     " the line and multiline comments are ignored until the outer-most '!#' (much like braces" +
                     " in C-like languages).";
        
        return Utils.createParagraph (text, { code });
    }
];

const functions =
[
    p1 = () =>
    {
        const code = "call print <- \"Hello World!\"\n" +
                     "print <- \"Invalid!\" # This acts like a reassignment";
        const text = "";
        
        return Utils.createParagraph (text, { title: "Calling Functions", code });
    },
    
    p2 = () =>
    {
        const code = "begin function hello\n" +
                     "&emsp;call print <- \"Hello World!\"\n" +
                     "end function";
        const text = "";
        
        return Utils.createParagraph (text, { title: "Declaring Functions", code });
    }
];

const operators =
[
    p1 = () => 
    {
        const table = Utils.createTable 
        (
            [ "Precedence", "Name", "Operator", "Operable Types", "Associativity", "Example" ],
            [
                [ "1", "Logical NOT", "!", "bool, float, int", "Right-to-left", "<code>!true # false</code>" ],
                [ "1", "Bitwise NOT", "~", "bool, float, int", "Right-to-left", "<code>~0 # -1</code>" ],
                [ "1", "Absolute", "+", "float, int", "Right-to-left", "<code>+(-5) # 5</code>" ],
                [ "1", "Negate", "-", "float, int", "Right-to-left", "<code>-(5) # -5</code>" ],
                [ "2", "Power", "**", "float, int", "Left-to-right", "<code>3 ** 2 # 9</code>" ],
                [ "3", "Multiplication", "*", "float, int", "Left-to-right", "<code>2.5 * 2 # 5.0</code>" ],
                [ "3", "Division (float)", "/", "float, int", "Left-to-right", "<code>3 / 2 # 1.5</code>" ],
                [ "3", "Division (int)", "//", "float, int", "Left-to-right", "<code>3.0 // 2.0 # 1</code>" ],
                [ "3", "Modulo (aka remainder)", "%", "float, int", "Left-to-right", "<code>2.5 % 1.3 # 1.2</code>" ],
                [ "4", "Addition", "+", "float, int", "Left-to-right", "<code>2 + 3 # 5</code>" ],
                [ "4", "Subtraction", "-", "float, int", "Left-to-right", "<code>3 - 7 # -4</code>" ]
            ],
            "Right"
        );
        const text = "This table shows Eir's available operators:\n\n" +
                     "The table is ordered by precedence, in descending order.";
        
        return Utils.createParagraph (text, { table });
    }
];

const types =
[
    
];

const variables =
[
    p1 = () =>
    {
        const code = "let x\n" +
                     "let y <- 2";
        const text = "Variables can be declared in a variety of ways. The code on the right shows the simplest." +
                     " The first line simply declares a variable called <code>x</code> with no initial value set" +
                     " to it. The second declares another variable called <code>y</code> with an initial integer" +
                     " value of 2.";
        
        return Utils.createParagraph (text, { title: "Declaring Variables", code });
    },
    
    p2 = () =>
    {
        const code = "const x <- \"Hello World!\"\n" +
                     "const y # Invalid!";
        const text = "Variables declared with the <code>let</code> keyword are mutable. If that is not desired, variables" +
                     " can be declared as immutable by declaring them with the <code>const</code> keyword. Note that when" +
                     " declaring immutable variables, you must initialize that variable in the same line or an error will" +
                     " occur. Thus the second line is not valid.";
        
        return Utils.createParagraph (text, { code });
    },
    
    p3 = () =>
    {
        const code = "int x <- 5\n" +
                     "x <- \"Nope!\" # Invalid!\n\n" +
                     "int const z <- 3\n" +
                     "const z <- 3";
        const text = "Optionally, you can specify the type when a variable is declared (types will be discussed in more" +
                     " detail soon). To do so, replace the <code>let</code> or <code>const</code> keyword with the desired" +
                     " type. Doing so disallows that variable from switching types, which is why the second line would throw" +
                     " an error. You don't have to drop the <code>const</code> keyword if you would still like the variable" +
                     " to be immutable, but this is completely optional as immutable variables can't change type regardless." +
                     " So, line 4 and 5 are identical in function.";
        
        return Utils.createParagraph (text, { code });
    },
    
    p4 = () =>
    {
        const code = "let x <- 5\n" +
                     "let x <- 2\n" +
                     "x # Would evaluate to 2";
        const text = "Eir allows you to redeclare variables. Doing so only allows you to access the value of the most recently" +
                     " declared variable, which is why line 3 would evaluate to 2.";
        
        return Utils.createParagraph (text, { title: "Shadowing Variables", code });
    }
];

[
    { header: "Variables", paragraphs: variables },
    { header: "Comments", paragraphs: comments },
    { header: "Operators", paragraphs: operators },
    { header: "Types", paragraphs: types },
    { header: "Functions", paragraphs: functions }
].forEach (element => $("body").append (Utils.createSection (element.header, element.paragraphs.map (paragraph => paragraph ()))));
